# Kavalanche/Security
Simple security library for web applications.

## Usage

1. Require `kavalanche/security`.

    ```bash
    composer require kavalanche/security
    ```

2. Create `UserProvider`. Refer to [Custom UserProvider](#custom-userprovider) section.

3. Create instance of `Kavalanche\Security\Service\RememberUserService`.

3. Create instance of `AuthenticationProvider` and inject your `UserProvider` and `RememberUserService` into it.

    ```php
    $authenticationProvider = new Kavalanche\Security\Provider\AuthenticationProvider($userProvider, $rememberUserService);
    ```

4. Check if user is authenticated.

    ```php
    try {
        $user = $authenticationProvider->authenticate();
    } catch (Kavalanche\Security\Exception\SecurityException $ex) {
        // if you want to allow unauthenticated users, then assign false or null to $user
        
        // if you require user to be authenticated do as follows
        if (!$e instanceof Kavalanche\Security\Exception\UserNotAuthenticatedException) {
            // put message in flash session and redirect to user form
            // or do whatever your use case demands
        }
    }
    ```

	You can specify `redirect-path` in `security.yaml` or pass desired path to `$_SESSION['redirect-path']`. Default is `/`.
	
    By default expected login form field names are `email` and `password`.

    You can change them by creating a config file named `{app_root}/config/security.yaml` and setting these variables:
    - `login-form-identifier-field` for identifier field
    - `login-form-password-field` for password field

5. Don't forget to put `session_start()` at the beginning of your file.

## Custom UserProvider

If you want to user different UserProvider, you can create your. It must implement `Kavalanche\Security\Interface\UserProviderInterface`.

```php
class UserProvider implements Kavalanche\Security\Interface\UserProviderInterface {

    public function loadUser($identifier) {
    
        // Fetch your User
        // Don't forget which type of identifier you defined in `security.yaml`
        
        // You can create your own User class (it must implement Kavalanche\Security\UserInterface)
        if ($user instanceof Kavalanche\Security\UserInterface) {
            return $user;
        }

        throw new Kavalanche\Security\Exception\SecurityException('Invalid username.');
    }

}

```

You can configure identifier type in your `{app_root}/config/security.yaml` file by setting `identifier` variable. Possible values are: email, username.

## Remember user

From version `v1.1.0` there is a possibility to add `Remember me` checkbox to login form. This option sets a cookie in user's browser and logs him in automatically.

Default configuration:
- `login-form-remember-me-field`: "rememberme"
- `remember-me-cookie-lifetime`: 2592000 # 30 days

## Password reset

From version `v2.0.0` there is a new functionality of resetting user password. You can use it as follows:

1. Add route to form with `email` field (configurable)
2. Check default email template and replace it with your own if needed (`password-reset-mail-template` in configuration file)
3. Add route to send email that includes code:

```
# You must create a service for sending e-mails that implements Kavalanche\Security\Interfaces\MailerInterface
$passwordResetService = new Kavalanche\Security\Service\PasswordResetService($passwordResetRepo, $userProvider, $mailer);

# Form data is handled internally if you use correctly configured fields (please refer to parameters.yaml file)
$passwordResetService->processResetRequest();

# You can redirect user according to the output of this method
```

4. Add new password handling route that includes code:

```
# Form data is handled internally if you use correctly configured fields (please refer to parameters.yaml file)
$passwordResetService->resetPassword();

# You can redirect user according to the output of this method
```

## Other informations

- It's your obligation to ensure that the usernames are unique.
- You can use this library however you want. To secure the whole application or just some routes.
- You can add multiple roles to each user. Simply assign an array with roles or permissions with `$user->setRoles()` setter.

## To do

- Implement some kind of request abstraction to encapsulate Requests (symfony/http-foundation?)
- Add helper to check permissions
