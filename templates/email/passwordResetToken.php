<h2>Password reset link</h2>
<p>By clicking the following link you can set new password for {url}:</p>
<p><a href="{url}/password/set?token={token}">{url}/password/set?token={token}</a></p>
<p>Regards,<br>
    {url} Administrator</p>