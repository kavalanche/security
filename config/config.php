<?php

namespace Kavalanche\Security;

function config(string $key) {
    $config = yaml_parse_file(__DIR__ . '/parameters.yaml');
    $userConfigPath = __DIR__ . '/../../../../config/security.yaml';
    if (file_exists($userConfigPath)) {
        $userConfig = yaml_parse_file($userConfigPath);
        $config = array_merge($config, $userConfig);
    }

    if (!empty($config[$key])) {
        return $config[$key];
    }
}
