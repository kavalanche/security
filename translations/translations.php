<?php

namespace Kavalanche\Security;

function _t(string $key, string $lang = 'en') {
    $translation = yaml_parse_file(__DIR__ . '/' . $lang . '.yaml');

    if (!empty($translation[$key])) {
        return $translation[$key];
    }
}
