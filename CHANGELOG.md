# Changelog

## v2.0.3

Released 2024-02-05

- Fixes missing token error
- Fixes default redirect path

## v2.0.0

Released 2023-06-08

- Introduces breaking changes
- Replaces RememberUserProvider with RememberUserService
- Adds password reset functionality

## v1.1.3

Released 2023-06-07

- Adds root path to remember user cookie

## v1.1.2

Released 2023-06-07

- Fixes wrong comparison left accidentally after tests

## v1.1.1

Released 2023-06-07

- Adds check for session status

## v1.1.0

Released 2023-06-02

- Added remember user option

## v1.0.1

Released 2023-01-04

- Added optional redirect path after successful login

## v1.0.0

Released 2022-02-27

- Introduces breaking changes
- Requires php >=7.4
- Added strict typing
- Adds error translations
- Adds HTTP status codes
- Changes directory structure in src
- Renames parameters in config
- Removes built in User providers
- Removes docker and public files

## v0.8.0

Released 2021-02-28

- Added redirect after sucessful login
- Added redirect_path to config
- Removed loadByUsername()
- Removed InvalidUserDataException

## v0.7.0

Released 2021-02-22

- Added configuration
- Added possibility to choose user identifier for authentication purpose
- Deprecated loadByUsername()
- Deprecated InvalidUserDataException

## v0.6.3

Released 2019-10-15

- Updated readme
- Fixed public demo

## v0.6.2

Released 2019-10-15

- Updated readme
- Changed demo in public

## v0.6.1

Released 2019-10-15

- Removes unused use statement

## v0.6.0

Released 2019-10-15

- Added library specific exceptions
- Fixed some typos

## v0.5.0

Released 2019-10-15

- Changed method of storing users in memory
- Changed implementation of InMemoryUserProvider
- Updated tests

## v0.4.0

Released 2019-10-14

- Changed InMemoryUserProvider to use data injected into constructor instead of hardcoded dataset

## v0.3.0

Released 2019-10-11

- Added strict types
- Added UserInterface

## v0.2.0

Released 2019-10-10

- Changed InMemoryUserProvider
- Added DatabaseUserProvider

## v0.1.0

Released 2019-10-09

- Initial release for testing purposes
