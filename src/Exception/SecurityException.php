<?php

declare(strict_types=1);

namespace Kavalanche\Security\Exception;

/**
 * @author Wojciech Burda <wojciech.burda@rp.pl>
 */
class SecurityException extends \Exception {
    
}
