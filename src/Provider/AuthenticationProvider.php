<?php

declare(strict_types=1);

namespace Kavalanche\Security\Provider;

use Kavalanche\Security\Exception\SecurityException;
use Kavalanche\Security\Interfaces\Provider\UserProviderInterface;
use Kavalanche\Security\Interfaces\Token\RememberUserTokenInterface;
use Kavalanche\Security\Interfaces\UserInterface;
use Kavalanche\Security\Service\RememberUserService;
use function Kavalanche\Security\_t;
use function Kavalanche\Security\config;

/**
 * @author Wojciech Burda <wojciech.burda@rp.pl>
 */
class AuthenticationProvider {

    private UserProviderInterface $userProvider;
    private RememberUserService $rememberUserService;

    public function __construct(UserProviderInterface $userProvider, RememberUserService $rememberUserService) {
        $this->userProvider = $userProvider;
        $this->rememberUserService = $rememberUserService;
    }

    public function authenticate(): ?UserInterface {
        $user = null;
        $identifier = config('identifier');
        $identifierFormField = config('login-form-identifier-field');
        $passwordFormField = config('login-form-password-field');
        $rememberMeFormField = config('login-form-remember-me-field');
        if (!in_array($identifier, ['email', 'username'], true)) {
            throw new SecurityException(_t('security.error.bad-identifier', config('lang')));
        }

        if (session_status() === PHP_SESSION_ACTIVE && !empty($_SESSION[$identifier])) {
            $user = $this->userProvider->loadUser($_SESSION[$identifier]);
            $this->saveUserInSession($user);
            return $user;
        }

        if (!empty($_COOKIE[$rememberMeFormField])) {
            $token = $this->rememberUserService->checkRememberUserToken();
            if ($token) {
                $user = $this->userProvider->loadUserById($token->getUserId());
                $this->saveUserInSession($user);
                return $user;
            }
        }

        if (!empty($_POST[$identifierFormField])) {
            $user = $this->userProvider->loadUser($_POST[$identifierFormField]);
            if (password_verify($_POST[$passwordFormField], $user->getPassword())) {
                if (!empty($_POST[$rememberMeFormField])) {
                    $this->rememberUserService->setRememberUserToken($user->getId());
                }
                $this->saveUserInSession($user);
                return $user;
            }
        }
        return null;
    }

    private function saveUserInSession(UserInterface $user): void {
        $identifier = config('identifier');
        if ($identifier === 'email') {
            $_SESSION[$identifier] = $user->getEmail();
        } elseif ($identifier === 'username') {
            $_SESSION[$identifier] = $user->getUsername();
        }
    }

    public function redirectAfterLogin(): void {
        $identifier = config('identifier');
        if (session_status() === PHP_SESSION_ACTIVE && !empty($_SESSION[$identifier])) {
            $redirectPath = $_SESSION['redirect-path'] ?? config('redirect-path');
            header('Location: ' . $redirectPath);
            $_SESSION['redirect-path'] = null;
            exit;
        }
    }

    /**
     * Checks if User has role needed to access given route.
     * 
     * Route must be defined as regular expression.
     * Examples:
     * - ^/$ - Matches only "/"
     * - ^/admin - Matches "/admin*"
     * 
     * @param UserInterface $user
     * @return bool
     */
    public function isAuthorizedToAccessRoute(UserInterface $user): bool {
        $accessList = config('access-list');
        $requestUri = parse_url($_SERVER['REQUEST_URI']);
        foreach ($accessList as $item) {
            $path = str_replace('/', '\/', $item['path']);
            $path = '/' . $path . '/';
            if (preg_match($path, $requestUri['path']) === 1 && count(array_intersect($user->getRoles(), $item['roles']))) {
                return true;
            }
        }
        return false;
    }

    public function getCurrentUser(): ?UserInterface {
        $identifier = config('identifier');
        $rememberMeFormField = config('login-form-remember-me-field');

        if (session_status() === PHP_SESSION_ACTIVE && !empty($_SESSION[$identifier])) {
            return $this->userProvider->loadUser($_SESSION[$identifier]);
        }

        if (!empty($_COOKIE[$rememberMeFormField])) {
            $token = $this->rememberUserService->checkRememberUserToken();
            if ($token) {
                return $this->userProvider->loadUserById($token->getUserId());
            }
        }

        return null;
    }

    /**
     * @todo: Add redirect location to config
     */
    public function logout(): void {
        if (!empty($_COOKIE[config('login-form-remember-me-field')])) {
            $incomingToken = $_COOKIE[config('login-form-remember-me-field')];
            $token = $this->rememberUserService->getToken($incomingToken);
            if ($token instanceof RememberUserTokenInterface) {
                $this->rememberUserService->deleteToken($token);
            }
            setcookie(config('login-form-remember-me-field'), '', time() - 3600, '/');
        }
        session_destroy();
        header('Location: /');
        exit;
    }
}
