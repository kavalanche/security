<?php

declare(strict_types=1);

namespace Kavalanche\Security\Service;

use Kavalanche\Security\Entity\RememberUserToken;
use Kavalanche\Security\Interfaces\Repository\RememberUserTokenRepositoryInterface;
use Kavalanche\Security\Interfaces\Token\RememberUserTokenInterface;
use function Kavalanche\Security\config;

/**
 * @author wojtek
 */
class RememberUserService {

    private RememberUserTokenRepositoryInterface $rememberUserRepository;

    public function __construct(RememberUserTokenRepositoryInterface $rememberUserRepository) {
        $this->rememberUserRepository = $rememberUserRepository;
    }

    public function setRememberUserToken(int $userId): RememberUserTokenInterface {
        $token = $this->generateToken();
        $expirationTime = time() + config('remember-me-cookie-lifetime');
        setcookie(config('login-form-remember-me-field'), $token, $expirationTime, '/');
        $rememberUserToken = new RememberUserToken();
        $rememberUserToken->setExpirationTime($expirationTime);
        $rememberUserToken->setToken($token);
        $rememberUserToken->setUserId($userId);
        $this->rememberUserRepository->storeToken($rememberUserToken);
        return $rememberUserToken;
    }

    public function checkRememberUserToken(): ?RememberUserTokenInterface {
        $incomingToken = $_COOKIE[config('login-form-remember-me-field')];
        $token = $this->rememberUserRepository->getToken($incomingToken);
        if ($token && $token->getExpirationTime() >= time()) {
            return $this->reloadRememberUserToken($token);
        }
        return null;
    }

    public function getToken(string $token): ?RememberUserTokenInterface {
        return $this->rememberUserRepository->getToken($token);
    }

    public function deleteToken(RememberUserTokenInterface $token) {
        $this->rememberUserRepository->deleteToken($token);
    }

    private function reloadRememberUserToken(RememberUserTokenInterface $token): RememberUserTokenInterface {
        $this->rememberUserRepository->deleteToken($token);
        return $this->setRememberUserToken($token->getUserId());
    }

    private function generateToken(int $length = 20): string {
        return bin2hex(random_bytes($length));
    }
}
