<?php

declare(strict_types=1);

namespace Kavalanche\Security\Service;

use Kavalanche\Security\Entity\PasswordResetToken;
use Kavalanche\Security\Interfaces\MailerInterface;
use Kavalanche\Security\Interfaces\Provider\UserProviderInterface;
use Kavalanche\Security\Interfaces\Repository\PasswordResetTokenRepositoryInterface;
use Kavalanche\Security\Interfaces\Token\PasswordResetTokenInterface;
use function Kavalanche\Security\config;

/**
 * @author Wojciech Burda <w.lk@wp.pl>
 */
class PasswordResetService {

    private PasswordResetTokenRepositoryInterface $passwordResetRepo;
    private UserProviderInterface $userProvider;
    private MailerInterface $mailer;

    public function __construct(PasswordResetTokenRepositoryInterface $passwordResetRepo, UserProviderInterface $userProvider, MailerInterface $mailer) {
        $this->passwordResetRepo = $passwordResetRepo;
        $this->userProvider = $userProvider;
        $this->mailer = $mailer;
    }

    public function processResetRequest(): bool {
        $user = $this->userProvider->loadUser(filter_var($_POST[config('password-reset-email-field')], FILTER_SANITIZE_EMAIL));
        $token = $this->setResetToken($user->getId());
        return $this->sendResetToken($token, $user->getEmail());
    }

    public function resetPassword(): bool {
        $newPassword = $_POST[config('password-reset-password-field')];
        if ($newPassword === $_POST[config('password-reset-password-confirm-field')]) {
            $token = $this->passwordResetRepo->getToken($_POST[config('password-reset-token-field')]);
            if ($token && $token->getExpirationTime() >= time()) {
                $this->passwordResetRepo->deleteToken($token);
                return $this->passwordResetRepo->changePassword($token->getUserId(), $newPassword);
            }
        }
        return false;
    }

    private function sendResetToken(PasswordResetTokenInterface $token, string $email): bool {
        $url = $_SERVER['HTTP_HOST'];
        $body = file_get_contents(__DIR__ . '/../../templates/email/passwordResetToken.php');
        if (file_exists(__DIR__ . '/../../../../../' . config('password-reset-mail-template') . '.php')) {
            $body = file_get_contents(__DIR__ . '/../../../../../' . config('password-reset-mail-template') . '.php');
        }
        $search = ['{url}', '{token}'];
        $replace = [$_SERVER['HTTP_HOST'], $token->getToken()];
        $body = str_replace($search, $replace, $body);

        return $this->mailer->send(config('password-reset-mail-from'), $email, config('password-reset-mail-subject'), $body, config('password-reset-mail-headers'));
    }

    private function setResetToken(int $userId): PasswordResetTokenInterface {
        $token = $this->generateToken();
        $expirationTime = time() + config('password-reset-token-lifetime');
        $passwordResetToken = new PasswordResetToken();
        $passwordResetToken->setExpirationTime($expirationTime);
        $passwordResetToken->setToken($token);
        $passwordResetToken->setUserId($userId);
        $this->passwordResetRepo->storeToken($passwordResetToken);
        return $passwordResetToken;
    }

    private function generateToken(int $length = 20): string {
        return bin2hex(random_bytes($length));
    }
}
