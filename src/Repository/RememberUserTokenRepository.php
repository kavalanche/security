<?php

declare(strict_types=1);

namespace Kavalanche\Security\Repository;

use Kavalanche\Security\Entity\RememberUserToken;
use Kavalanche\Security\Interfaces\Repository\RememberUserTokenRepositoryInterface;
use Kavalanche\Security\Interfaces\Token\RememberUserTokenInterface;
use PDO;
use stdClass;
use function Kavalanche\Security\config;

/**
 * @author Wojciech Burda <w.lk@wp.pl>
 */
class RememberUserTokenRepository implements RememberUserTokenRepositoryInterface {

    protected PDO $db;
    private string $tableName;

    public function __construct(PDO $db) {
        $this->db = $db;
        $this->tableName = config('remember-user-token-table-name');
    }

    public function deleteToken(RememberUserTokenInterface $token): void {
        $sql = "DELETE FROM " . $this->tableName . " WHERE token = :token";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'token' => $token->getToken()
        ]);
    }

    public function getToken(string $token): ?RememberUserTokenInterface {
        $sql = "SELECT * FROM " . $this->tableName . " WHERE token = :token";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'token' => $token
        ]);
        $rememberUserToken = $stmt->fetchObject() ?: null;
        if ($rememberUserToken instanceof stdClass) {
            $rememberUserToken = $this->hydrate($rememberUserToken);
        }

        return $rememberUserToken;
    }

    public function storeToken(RememberUserTokenInterface $token): void {
        $sql = "INSERT INTO " . $this->tableName . " ("
                . "token,"
                . "expiration_time,"
                . "user_id"
                . ") VALUES ("
                . ":token,"
                . ":expiration_time,"
                . ":user_id"
                . ")";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'token' => $token->getToken(),
            'expiration_time' => $token->getExpirationTime(),
            'user_id' => $token->getUserId()
        ]);
    }

    private function hydrate(stdClass $object): RememberUserTokenInterface {
        $rememberUserToken = new RememberUserToken();
        $rememberUserToken->setExpirationTime($object->expiration_time);
        $rememberUserToken->setToken($object->token);
        $rememberUserToken->setUserId($object->user_id);

        return $rememberUserToken;
    }
}
