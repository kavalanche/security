<?php

declare(strict_types=1);

namespace Kavalanche\Security\Repository;

use Kavalanche\Security\Entity\PasswordResetToken;
use Kavalanche\Security\Interfaces\Repository\PasswordResetTokenRepositoryInterface;
use Kavalanche\Security\Interfaces\Token\PasswordResetTokenInterface;
use PDO;
use stdClass;
use function Kavalanche\Security\config;

/**
 * @author Wojciech Burda <w.lk@wp.pl>
 */
class PasswordResetTokenRepository implements PasswordResetTokenRepositoryInterface {

    protected PDO $db;
    private string $tableName;
    private string $userTableName;

    public function __construct(PDO $db) {
        $this->db = $db;
        $this->tableName = config('password-reset-token-table-name');
        $this->userTableName = config('user-table-name');
    }

    public function deleteToken(PasswordResetTokenInterface $token): void {
        $sql = "DELETE FROM " . $this->tableName . " WHERE token = :token";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'token' => $token->getToken()
        ]);
    }

    public function getToken(string $token): ?PasswordResetTokenInterface {
        $sql = "SELECT * FROM " . $this->tableName . " WHERE token = :token";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'token' => $token
        ]);
        $rememberUserToken = $stmt->fetchObject() ?: null;
        if ($rememberUserToken instanceof stdClass) {
            $rememberUserToken = $this->hydrate($rememberUserToken);
        }

        return $rememberUserToken;
    }

    public function storeToken(PasswordResetTokenInterface $token): void {
        $sql = "INSERT INTO " . $this->tableName . " ("
                . "token,"
                . "expiration_time,"
                . "user_id"
                . ") VALUES ("
                . ":token,"
                . ":expiration_time,"
                . ":user_id"
                . ")";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'token' => $token->getToken(),
            'expiration_time' => $token->getExpirationTime(),
            'user_id' => $token->getUserId()
        ]);
    }

    private function hydrate(stdClass $object): PasswordResetTokenInterface {
        $rememberUserToken = new PasswordResetToken();
        $rememberUserToken->setExpirationTime($object->expiration_time);
        $rememberUserToken->setToken($object->token);
        $rememberUserToken->setUserId($object->user_id);

        return $rememberUserToken;
    }

    public function changePassword(int $userId, string $password): bool {
        $sql = "UPDATE " . $this->userTableName . " SET password = :password WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute([
                    'password' => password_hash($password, PASSWORD_DEFAULT),
                    'id' => $userId,
        ]);
    }
}
