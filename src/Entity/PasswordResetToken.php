<?php

declare(strict_types=1);

namespace Kavalanche\Security\Entity;

use Kavalanche\Security\Interfaces\Token\PasswordResetTokenInterface;

/**
 * @author Wojciech Burda <w.lk@wp.pl>
 */
class PasswordResetToken implements PasswordResetTokenInterface {

    private int $expirationTime;
    private string $token;
    private int $userId;

    public function getExpirationTime(): int {
        return $this->expirationTime;
    }

    public function getToken(): string {
        return $this->token;
    }

    public function getUserId(): int {
        return $this->userId;
    }

    public function setExpirationTime(int $expirationTime): void {
        $this->expirationTime = $expirationTime;
    }

    public function setToken(string $token): void {
        $this->token = $token;
    }

    public function setUserId(int $userId): void {
        $this->userId = $userId;
    }
}
