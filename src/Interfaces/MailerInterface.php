<?php

declare(strict_types=1);

namespace Kavalanche\Security\Interfaces;

/**
 * @author wojtek
 */
interface MailerInterface {

    public function send(string $from, string $to, string $subject, string $body, array $headers): bool;
}
