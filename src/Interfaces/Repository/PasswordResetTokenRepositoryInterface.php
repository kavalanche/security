<?php

declare(strict_types=1);

namespace Kavalanche\Security\Interfaces\Repository;

use Kavalanche\Security\Interfaces\Token\PasswordResetTokenInterface;

/**
 * @author Wojciech Burda <w.lk@wp.pl>
 */
interface PasswordResetTokenRepositoryInterface {

    public function getToken(string $token): ?PasswordResetTokenInterface;

    public function storeToken(PasswordResetTokenInterface $token): void;

    public function deleteToken(PasswordResetTokenInterface $token): void;

    public function changePassword(int $userId, string $password): bool;
}
