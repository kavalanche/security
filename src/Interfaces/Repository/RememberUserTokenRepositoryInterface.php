<?php

declare(strict_types=1);

namespace Kavalanche\Security\Interfaces\Repository;

use Kavalanche\Security\Interfaces\Token\RememberUserTokenInterface;


/**
 * @author Wojciech Burda <w.lk@wp.pl>
 */
interface RememberUserTokenRepositoryInterface {

    public function getToken(string $token): ?RememberUserTokenInterface;

    public function storeToken(RememberUserTokenInterface $token): void;

    public function deleteToken(RememberUserTokenInterface $token): void;
}
