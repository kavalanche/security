<?php

declare(strict_types=1);

namespace Kavalanche\Security\Interfaces\Provider;

use Kavalanche\Security\Interfaces\UserInterface;

/**
 * @author Wojciech Burda <wojciech.burda@rp.pl>
 */
interface UserProviderInterface {

    public function loadUser(string $identifier): UserInterface;

    public function loadUserById(int $id): UserInterface;
}
