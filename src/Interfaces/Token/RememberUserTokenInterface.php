<?php

declare(strict_types=1);

namespace Kavalanche\Security\Interfaces\Token;

/**
 * @author Wojciech Burda <w.lk@wp.pl>
 */
interface RememberUserTokenInterface {

    public function getUserId(): int;

    public function getToken(): string;

    public function getExpirationTime(): int;
}
