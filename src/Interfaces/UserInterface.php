<?php

declare(strict_types=1);

namespace Kavalanche\Security\Interfaces;

/**
 * @author Wojciech Burda <wojciech.burda@rp.pl>
 */
interface UserInterface {

    public function getUsername(): string;

    public function getEmail(): string;

    public function getPassword(): string;

    public function getRoles(): array;
}
